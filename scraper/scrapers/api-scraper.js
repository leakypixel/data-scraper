const http = require('http');
const fs = require('careless-fs');
const logError = require('simple-node-logger').createSimpleLogger('/data/error.log');
const MONTH_NAMES = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

const options = {
  hostname: '10.10.10.61',
  port: 5001,
  path: '/api/v1/tyres?section=215&profile=55&rim=16&vrm=HX64VCE&postcode=le671tu&load=97&speed=&originator=123456&searchConsumer=1',
  method: 'GET',
  headers: {
    'Authorization': 'BASIC aWQ6c2VjcmV0'
  }
};

const keys = ["Items", "ProductTyreId", "WarehouseId", "LeadTime", "IsFromOurStock", "SellPrice", "SellPriceExVAT", "SellPriceIncVAT", "VATAmount", "VATRate", "SingleTyreSupplement"];
const headers = "Product Tyre ID, Warehouse ID, Lead Time, Is From Our Stock?, Sell Price ex VAT, Sell Price inc VAT, VAT Amount, VAT Rate, Single Tyre Supplement\n";

const req = http.request(options, (res) => {
  let data = '';
  res.setEncoding('utf8');
  res.on('data', (chunk) => {
    data += chunk;
  });
  res.on('end', () => {
    data = JSON.parse(data);
    
    // Use stringify to only select the keys we're interested in, we'll save the JSON for programmatic use later.
    let jsonDataString = JSON.stringify(data, keys, "  ");
    
    // Coerce JSON to CSV with headers for human-readable output.
    let csvDataString = data.Items.map((item) => {
      return `${item.ProductTyreId}, ${item.WarehouseId}, ${item.LeadTime}, ${item.IsFromOurStock}, ${item.SellPrice.SellPriceExVAT}, ${item.SellPrice.SellPriceIncVAT}, ${item.SellPrice.VATAmount}, ${item.SellPrice.VATRate}, ${item.SellPrice.SingleTyreSupplement}`;
    });
    csvDataString = `${headers}${csvDataString.join("\n")}`;
    
    // Get date so we can have a sane directory structure
    let currentDate = new Date();
    let datePath = `/data/${currentDate.getFullYear()}/${MONTH_NAMES[currentDate.getMonth()]}/${currentDate.getDate()}/${currentDate.getHours()}:${currentDate.getMinutes() < 10 ? '0':''}${currentDate.getMinutes()}`;

    // Save files
    fs.write({path: `${datePath}-data.json`, data: jsonDataString});
    fs.write({path: `${datePath}-data.csv`, data: csvDataString});
  });
});

req.on('error', (e) => {
  logError.error(`problem with request: ${e.message}`);
});

req.end();
