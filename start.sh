if [ -z "$1" ]; then
  echo "Please specify the data directory as the only argument to this script."
  exit
fi

docker build -t http-server serve-data/
docker build -t scraper scraper/

docker run -itdv $1:/data scraper
docker run -itdv $1:/web -p 80:8000 http-server
